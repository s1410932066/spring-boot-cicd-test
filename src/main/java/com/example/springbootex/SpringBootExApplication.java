package com.example.springbootex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
//@RestController //網址的進入點
public class SpringBootExApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootExApplication.class, args);
    }


}
