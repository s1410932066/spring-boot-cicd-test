package com.example.springbootex.service;

import com.example.springbootex.User;
import com.example.springbootex.repository.MyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyService {

    @Autowired
    MyRepository repository;

    public void adduser(User user) throws IllegalAccessException{
        if (user.getAccount()!= null || user.getPassword()!= null){
            repository.save(user);
        }else {
            throw new IllegalAccessException("file");
        }
    }

//    public List<User> getAllUser(){
//       return repository.findAll();
//    }

//    public List<User> getUserById(Long id){
//        if (repository.findUserById(id).isPresent()){
//            return repository.findUserById(id).get();
//        }
//        return repository.findAll();
//    }
    public List<User> getUserByAccount(String account){
        if(repository.findUserByAccount(account).isPresent()) {
            repository.findUserByAccount(account).get();
        }
        return repository.findUserByAccount(account).get();
    }
////
//    public List<User> getUserByPassword(String password){
//        return repository.findUserByPassword(password).get();
//    }
}
