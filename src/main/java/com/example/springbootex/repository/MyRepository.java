package com.example.springbootex.repository;

import com.example.springbootex.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MyRepository extends JpaRepository<User, Long> {
    Optional<List<User>> findUserById(Long id);
    Optional<List<User>> findUserByAccount(String account);
//    Optional<List<User>> findUserByPassword(String password);

}
