package com.example.springbootex.Controller;

import com.example.springbootex.User;
import com.example.springbootex.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class MyController {


    @Autowired
    MyService service;


    @GetMapping("/hello")
    public  String  hello(){
        return "Hello";
    }


    @PostMapping("/user")       //Param方式輸入
    public ResponseEntity<?> user(@RequestParam String account, @RequestParam String password){
        User user = User
                .builder()
                .account(account)
                .password(password)
                .build();
        try {
            service.adduser(user);
            return ResponseEntity.ok("good");
        }catch (Exception e){
            return ResponseEntity.badRequest().body("no");
        }
    }

    @PostMapping("/userBody")  //Body方式輸入
    public ResponseEntity<?> user(@RequestBody User user){
        try {
            service.adduser(user);
            return ResponseEntity.ok("good");
        }catch (Exception e){
            return ResponseEntity.badRequest().body("no");
        }
    }
//    @GetMapping("/userFind")
//    public ResponseEntity<?> user(@RequestParam String account){
//        try {
//            return ResponseEntity.ok(service.getUserByAccount(account));
//        }catch (Exception e){
//            return ResponseEntity.badRequest().body("no");
//        }
//    }

    //抓資料庫中的資料
    @GetMapping("/userName")
    public ResponseEntity<?> user(@RequestParam String account){
        try {
//            if (!service.getUserById(id).equals("")){
//
//            }
//            service.getUserById(id);
//            service.getUserByAccount(account);
//            service.getUserByPassword(password);
            return ResponseEntity.ok(service.getUserByAccount(account));

        }catch (Exception e){
            return ResponseEntity.badRequest().body("no");
        }

    }
}
