# 第一階段：構建JAR文件
FROM maven:3.8.4-openjdk-17 AS builder
WORKDIR /app
COPY . .
RUN mvn clean package

# 第二階段：將JAR文件複製到最終映像
FROM openjdk:17-jdk
WORKDIR /app
#複製第一階段到當前階段
COPY --from=builder /app/target/SpringBootEx-0.0.1-SNAPSHOT.jar app.jar
CMD ["java", "-jar", "app.jar"]


## 使用 OpenJDK 11 作為基礎映像檔
#FROM openjdk:17
#
## 設定工作目錄
#WORKDIR /app
#
## 複製打包好的 Spring Boot JAR 檔到容器內
#COPY SpringBootEx-0.0.1-SNAPSHOT.jar app.jar
#
## 暴露 Spring Boot 應用程式的預設埠號（例如：8080）
#EXPOSE 8080
#
## 定義容器啟動時要執行的指令
#CMD ["java", "-jar", "app.jar"]